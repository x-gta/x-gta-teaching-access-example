# X-GTA Teaching Access Example

This repository contains code examples of how students can access the X-GTA Teaching Corpus.

**Important:** Copy (or rename) `config_example.yml` to `config.yml`, then replace the placeholder strings with **your own *private***  `user` and `password` credentials that you received.